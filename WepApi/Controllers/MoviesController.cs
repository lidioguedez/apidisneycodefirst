﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Core.Interfaces;
using Core.DTO;
using BusinessLogic.Logic;
using Newtonsoft.Json;

namespace WepApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IPeliculaRepository _MovieRepository;

        public MoviesController(IPeliculaRepository MovieRepository)
        {
            _MovieRepository = MovieRepository;
        }

        [HttpGet]
        public async Task<ActionResult<List<PeliculaDto>>> index()
        {
            var movies = await _MovieRepository.GetPeliculasAsinc();

            return Ok(movies);
        }
    }

}
