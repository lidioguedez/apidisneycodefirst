﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IPersonajeRepository
    {
        Task<Personaje> GetPersonajeByIdAsinc(int id);
        Task<IReadOnlyList<Personaje>> GetPersonajesAsinc();
        Task<IReadOnlyList<Personaje>> GetPersonajesByNameAsinc(string name);
        Task<IReadOnlyList<Personaje>> GetPersonajesByAgeAsinc(string age);
        Task<IReadOnlyList<Personaje>> GetPersonajesByPictureAsinc(string idPicture);
        Task<Personaje> CreatePersonaje(Personaje personaje);
        Task<string> DeletePersonajeAsinc(int id);
        Task<Personaje> EditPersonajeAsinc(int id);

    }
}
