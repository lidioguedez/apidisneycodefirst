﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IGeneroRepository
    {
        Task<Genero> GetGeneroByIdAsinc(int id);
        Task<IReadOnlyList<Genero>> GetGenerosAsinc();
        Task<Genero> CreateGenero(Pelicula Pelicula);
        Task<string> DeleteGeneroAsinc(int id);
        Task<Genero> EditGeneroAsinc(int id);

    }
}
