﻿using Core.Entities;
using Core.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IPeliculaRepository
    {
        Task<Pelicula> GetPeliculaByIdAsinc(int id);
        Task<IReadOnlyList<PeliculaDto>> GetPeliculasAsinc();
        Task<IReadOnlyList<Pelicula>> GetPeliculasByNameAsinc(string name);
        Task<IReadOnlyList<Pelicula>> GetPeliculasByGenreAsinc(string genre);
        Task<Pelicula> CreatePelicula(Pelicula Pelicula);
        Task<string> DeletePeliculaAsinc(int id);
        Task<Pelicula> EditPeliculaAsinc(int id);
    }
}
