﻿using AutoMapper;
using BusinessLogic.Data;
using Core.DTO;
using Core.Entities;
using Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Logic
{
    public class PeliculaRepository : IPeliculaRepository
    {
        private readonly ApiDisneyDbContext DbContext;
        private readonly IMapper _mapper;
        public PeliculaRepository(ApiDisneyDbContext _context, IMapper mapper)
        {
            DbContext = _context;
            _mapper = mapper;
        }

        public async Task<Pelicula> CreatePelicula(Pelicula Pelicula)
        {
            await DbContext.Peliculas.AddAsync(Pelicula);
            DbContext.SaveChanges();

            return Pelicula;
        }

        public Task<string> DeletePeliculaAsinc(int id)
        {
            throw new NotImplementedException();
        }

        public Task<Pelicula> EditPeliculaAsinc(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<Pelicula> GetPeliculaByIdAsinc(int id)
        {
            return await DbContext.Peliculas.FindAsync(id); 
        }

        public async Task<IReadOnlyList<PeliculaDto>> GetPeliculasAsinc()
        {
            var pelis = await DbContext.Peliculas
                            .Include(g => g.Genero)
                            .Include(p => p.Personajes).ThenInclude(pi => pi.Personaje)
                            .ToListAsync();

            var peliculas =  _mapper.Map<List<Pelicula>, List<PeliculaDto>>(pelis);

            return peliculas;
        }

        public Task<IReadOnlyList<Pelicula>> GetPeliculasByGenreAsinc(string genre)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<Pelicula>> GetPeliculasByNameAsinc(string name)
        {
            throw new NotImplementedException();
        }
    }
}
