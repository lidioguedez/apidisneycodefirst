﻿using Core.Entities;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Logic
{
    public class GeneroRepository : IGeneroRepository
    {
        public Task<Genero> CreateGenero(Pelicula Pelicula)
        {
            throw new NotImplementedException();
        }

        public Task<string> DeleteGeneroAsinc(int id)
        {
            throw new NotImplementedException();
        }

        public Task<Genero> EditGeneroAsinc(int id)
        {
            throw new NotImplementedException();
        }

        public Task<Genero> GetGeneroByIdAsinc(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<Genero>> GetGenerosAsinc()
        {
            throw new NotImplementedException();
        }
    }
}
