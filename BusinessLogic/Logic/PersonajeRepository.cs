﻿using Core.Entities;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Logic
{
    public class PersonajeRepository : IPersonajeRepository
    {
        public Task<Personaje> CreatePersonaje(Personaje personaje)
        {
            throw new NotImplementedException();
        }

        public Task<string> DeletePersonajeAsinc(int id)
        {
            throw new NotImplementedException();
        }

        public Task<Personaje> EditPersonajeAsinc(int id)
        {
            throw new NotImplementedException();
        }

        public Task<Personaje> GetPersonajeByIdAsinc(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<Personaje>> GetPersonajesAsinc()
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<Personaje>> GetPersonajesByAgeAsinc(string age)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<Personaje>> GetPersonajesByNameAsinc(string name)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<Personaje>> GetPersonajesByPictureAsinc(string idPicture)
        {
            throw new NotImplementedException();
        }
    }
}
