﻿using AutoMapper;
using Core.DTO;
using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BusinessLogic
{
    internal class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Pelicula,PeliculaDto>().ForMember(d => d.data, x => x.MapFrom(src => src.Personajes));
            CreateMap<PeliculaPersonaje, PeliculaPersonajeDto>();
            CreateMap<Personaje, PersonajeDto>();
            CreateMap<Genero, GeneroDto>();

        }

    }
}
